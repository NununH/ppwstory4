from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.page, name='page'),
    path('', views.newpage, name='newpage'),
]
