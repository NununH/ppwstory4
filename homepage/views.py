from django.shortcuts import render

# Create your views here.
def page(request):
    return render(request, page.html)

def newpage(request):
    return render(request, newpage.html)
